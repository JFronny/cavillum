plugins {
    id("java")
    id("application")
    id("com.github.johnrengelman.shadow") version "7.0.0"
}

group = "io.gitlab.jfronny"
version = "1.0-SNAPSHOT"

application {
    mainClass.set("io.gitlab.jfronny.cavillum.Cavillum")
}

repositories {
    mavenCentral()
    maven { url = uri("https://oss.sonatype.org/content/repositories/snapshots/") }
    maven { url = uri("https://gitlab.com/api/v4/projects/35745143/packages/maven") }
}

val manifoldVersion = "2022.1.19"
val commonsVersion = "2022.9.15+12-51-21"

configurations {
    testImplementation.get().extendsFrom(annotationProcessor.get())
}

dependencies {
    //compileOnly("systems.manifold:manifold-rt:$manifoldVersion")
    implementation("systems.manifold:manifold-templates-rt:$manifoldVersion")
    implementation("io.gitlab.jfronny:commons:$commonsVersion")
    implementation("io.gitlab.jfronny:commons-gson:$commonsVersion")
    implementation("io.gitlab.jfronny:commons-slf4j:$commonsVersion")
    implementation("io.javalin:javalin:4.6.4")

    implementation("org.mariadb.jdbc:mariadb-java-client:3.0.6")
    implementation("org.jooq:jooq:3.17.4")
    implementation("org.slf4j:slf4j-api:2.0.0")

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.9.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.9.0")

    //annotationProcessor("systems.manifold:manifold-rt:$manifoldVersion")
    annotationProcessor("systems.manifold:manifold-templates:$manifoldVersion")
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

val modules = JavaVersion.current() != JavaVersion.VERSION_1_8 && sourceSets.main.get().allJava.files.any {it.name == "module-info.java"}
tasks.withType<JavaCompile> {
    options.compilerArgs.add("-Xplugin:Manifold")
    if (modules) {
        options.compilerArgs.add("--module-path")
        options.compilerArgs.add("it.classpath.asPath")
    }
    options.compilerArgs.add("-Amanifold.source.target=${project.buildDir}/manifoldDump")
}