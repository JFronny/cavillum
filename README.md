# Cavillum
Cavillum is in development and unfinished, this page mostly serves as a set of notes to myself

# Setup
- Install MariaDB
- Run the following commands to create a new database/user
  - CREATE DATABASE cavillum;
  - CREATE USER 'cavillum'@'localhost' IDENTIFIED BY 'some_pass';
  - GRANT ALL PRIVILEGES ON cavillum.* TO 'cavillum'@'localhost';
  - FLUSH PRIVILEGES;
- Set up the database
  - export CAVILLUM_DB_URL="jdbc:mariadb://localhost:3306/cavillum"
  - export CAVILLUM_DB_USER=cavillum
  - export CAVILLUM_DB_PASS=some_pass
  - export CAVILLUM_PORT=8080
  - java -jar Cavillum.jar

# Running
Set up the environment variables as in the `Set up the database` step except for CAVILLUM_DB_ACTION

# Formatting
Cavillum posts are formatted in a custom markdown-inspired format.
The following formatting is supported:
- Bold text: `*this is bold*`
- Italic text: `**this is italic**`
- Strikethrough text: `~~this is trikethrough**`
- Spoilers: `[spoiler]This is a spoiler[/spoiler]`
- Hyperlinks: `[link]https://example.com[/link]`
- Glowing text: `(((this text glows)))`
- Greentext: `> this text is green`