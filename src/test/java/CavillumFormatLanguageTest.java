import io.gitlab.jfronny.cavillum.formatter.CavillumFormatter;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CavillumFormatLanguageTest {
    @Test
    void formatterTest() {
        String source = """
                This is the first bit of source
                Is it not?""";
        assertEquals(source, CavillumFormatter.decompile(CavillumFormatter.parse(source)));
        assertEquals("""
                     This is the first bit of source<br>
                     Is it not?""", CavillumFormatter.build(source));
        source = """
                *This* is **the first bit of source**
                Is ~~it~~ not?
                > also this btw""";
        assertEquals(source, CavillumFormatter.decompile(CavillumFormatter.parse(source)));
        assertEquals("""
                <b>This</b> is <i>the first bit of source</i><br>
                Is <del>it</del> not?<br>
                <span class="unkfunc">&gt;\salso this btw</span>""", CavillumFormatter.build(source));
    }
}
