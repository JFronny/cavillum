package io.gitlab.jfronny.cavillum.model;

import java.sql.Timestamp;

public record Post(String username, String subject, String comment, String filename, Timestamp postTime, AttachmentType attachmentType) {
}
