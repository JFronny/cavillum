package io.gitlab.jfronny.cavillum.model;

import io.gitlab.jfronny.cavillum.Cfg;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public enum AttachmentType {
    JPEG("image/jpeg", Set.of("jpg", "jpeg"), AttachmentTypeGroup.IMAGE),
    PNG("image/png", Set.of("png"), AttachmentTypeGroup.IMAGE),
    GIF("image/gif", Set.of("gif"), AttachmentTypeGroup.IMAGE),
    MP3("audio/mpeg", Set.of("mp3"), AttachmentTypeGroup.AUDIO),
    MP4("video/mp4", Set.of("mp4"), AttachmentTypeGroup.VIDEO),
    WAV("audio/wav", Set.of("wav"), AttachmentTypeGroup.AUDIO),
    WEBM("video/webm", Set.of("webm"), AttachmentTypeGroup.VIDEO),
    YOUTUBE(null, Set.of(), AttachmentTypeGroup.YOUTUBE);

    private final String mimeType;
    private final Set<String> extensions;
    private final AttachmentTypeGroup group;

    private static final Map<String, AttachmentType> EXT_TO_TYPE;
    private static final Map<String, AttachmentType> MIME_TO_TYPE;

    static {
        Map<String, AttachmentType> tmp1 = new HashMap<>(), tmp2 = new HashMap<>();
        for (AttachmentType value : values()) {
            for (String extension : value.extensions) {
                tmp1.put(extension, value);
            }
            if (value.mimeType != null) tmp2.put(value.mimeType, value);
        }
        EXT_TO_TYPE = Map.copyOf(tmp1);
        MIME_TO_TYPE = Map.copyOf(tmp2);
    }

    public static AttachmentType getByExtension(String extension) {
        return EXT_TO_TYPE.get(extension.substring(extension.lastIndexOf('.') + 1));
    }

    public static AttachmentType getByMime(String mime) {
        return MIME_TO_TYPE.get(mime);
    }

    AttachmentType(String mimeType, Set<String> extensions, AttachmentTypeGroup group) {
        this.mimeType = mimeType;
        this.extensions = extensions;
        this.group = group;
    }

    public String getMimeType() {
        return mimeType;
    }

    public String getImageHTML(long postId, byte[] data) {
        return group.render(this == YOUTUBE
                    ? URLEncoder.encode(new String(data), StandardCharsets.UTF_8)
                    : Cfg.subpath + "post/" + postId + "/attachment",
                mimeType,
                "300");
    }

    enum AttachmentTypeGroup {
        AUDIO("<audio controls><source src=\"@source\" type=\"@type\">Your browser doesn't support audio</audio>"),
        VIDEO("<video width=\"@width\" controls><source src=\"@source\" type=\"@type\"></video>"),
        IMAGE("<img width=\"@width\" src=\"@source\" class=\"thumb\"></img>"),
        YOUTUBE("<iframe width=\"@width\" src=\"https://piped.kavin.rocks/embed/@source\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>");

        private final String template;

        AttachmentTypeGroup(String template) {
            this.template = template;
        }

        public String render(String source, String type, String width) {
            return template
                    .replaceAll("@source", source)
                    .replaceAll("@type", type)
                    .replaceAll("@width", width);
        }
    }
}
