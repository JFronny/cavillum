package io.gitlab.jfronny.cavillum.database.migration;

import org.jooq.DSLContext;

public abstract class Migration {
    private final int version;

    public Migration(int version) {
        this.version = version;
    }

    public abstract void perform(DSLContext dsl);

    public int getVersion() {
        return version;
    }
}
