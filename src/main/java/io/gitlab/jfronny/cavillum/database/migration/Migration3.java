package io.gitlab.jfronny.cavillum.database.migration;

import org.jooq.DSLContext;

import static io.gitlab.jfronny.cavillum.database.KnownFields.attachment;
import static io.gitlab.jfronny.cavillum.database.KnownFields.cv_posts;

public class Migration3 extends Migration {
    public Migration3() {
        super(3);
    }

    @Override
    public void perform(DSLContext dsl) {
        dsl.alterTable(cv_posts).dropColumn(attachment).execute();
    }
}
