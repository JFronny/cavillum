package io.gitlab.jfronny.cavillum.database;

import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Table;

import java.sql.Timestamp;

import static org.jooq.impl.DSL.*;
import static org.jooq.impl.SQLDataType.*;

public class KnownFields {
    public static final Field<Long> id = field(name("id"), BIGINT.identity(true));

    public static final Table<Record> cv_meta = table(name("cv_meta"));
    public static final Field<Integer> schema_version = field(name("schema_version"), INTEGER);

    public static final Table<Record> POST = table(name("POST"));
    public static final Field<byte[]> ATTACHMENT = field(name("ATTACHMENT"), BLOB);
    public static final Field<String> ATTACHMENT_TYPE = field(name("ATTACHMENT_TYPE"), VARCHAR(256));
    public static final Field<String> COMMENT = field(name("COMMENT"), LONGVARCHAR.null_());
    public static final Field<String> FILENAME = field(name("FILENAME"), VARCHAR(256));
    public static final Field<Timestamp> POST_TIME = field(name("POST_TIME"), TIMESTAMP.null_());
    public static final Field<String> SUBJECT = field(name("SUBJECT"), VARCHAR(256));
    public static final Field<String> USERNAME = field(name("USERNAME"), VARCHAR(50));

    public static final Table<Record> cv_posts = table(name("cv_posts"));
    public static final Field<byte[]> attachment = field(name("attachment"), BLOB);
    public static final Field<String> attachment_type = field(name("attachment_type"), VARCHAR(256));
    public static final Field<String> comment = field(name("comment"), LONGVARCHAR.null_());
    public static final Field<String> filename = field(name("filename"), VARCHAR(256));
    public static final Field<Timestamp> post_time = field(name("post_time"), TIMESTAMP.null_());
    public static final Field<String> subject = field(name("subject"), VARCHAR(256));
    public static final Field<String> username = field(name("username"), VARCHAR(50));
}
