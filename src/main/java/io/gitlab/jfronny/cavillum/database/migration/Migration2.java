package io.gitlab.jfronny.cavillum.database.migration;

import org.jooq.DSLContext;

import static io.gitlab.jfronny.cavillum.database.KnownFields.*;
import static io.gitlab.jfronny.cavillum.database.KnownFields.username;

public class Migration2 extends Migration {
    public Migration2() {
        super(2);
    }

    @Override
    public void perform(DSLContext dsl) {
        dsl.alterTable(POST).renameTo(cv_posts).execute();
        dsl.alterTable(cv_posts).renameColumn(ATTACHMENT).to(attachment).execute();
        dsl.alterTable(cv_posts).renameColumn(ATTACHMENT_TYPE).to(attachment_type).execute();
        dsl.alterTable(cv_posts).renameColumn(COMMENT).to(comment).execute();
        dsl.alterTable(cv_posts).renameColumn(FILENAME).to(filename).execute();
        dsl.alterTable(cv_posts).renameColumn(POST_TIME).to(post_time).execute();
        dsl.alterTable(cv_posts).renameColumn(SUBJECT).to(subject).execute();
        dsl.alterTable(cv_posts).renameColumn(USERNAME).to(username).execute();
    }
}
