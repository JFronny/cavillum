package io.gitlab.jfronny.cavillum.database.migration;

import org.jooq.DSLContext;

import static io.gitlab.jfronny.cavillum.database.KnownFields.*;

public class Migration1 extends Migration {
    public Migration1() {
        super(1);
    }

    @Override
    public void perform(DSLContext dsl) {
        dsl.createTableIfNotExists(POST)
                .column(id)
                .column(ATTACHMENT)
                .column(ATTACHMENT_TYPE)
                .column(COMMENT)
                .column(FILENAME)
                .column(POST_TIME)
                .column(SUBJECT)
                .column(USERNAME)
                .primaryKey(id)
                .execute();
    }
}
