package io.gitlab.jfronny.cavillum.database;

import io.gitlab.jfronny.cavillum.model.AttachmentType;
import io.gitlab.jfronny.cavillum.model.Post;
import org.jooq.Record;

import static io.gitlab.jfronny.cavillum.database.KnownFields.*;

public class TypeMappings {
    public static Post recordToPost(Record record) {
        if (record == null) return null;
        return new Post(
                record.get(username),
                record.get(subject),
                record.get(comment),
                record.get(filename),
                record.get(post_time),
                AttachmentType.valueOf(record.get(attachment_type))
        );
    }
}
