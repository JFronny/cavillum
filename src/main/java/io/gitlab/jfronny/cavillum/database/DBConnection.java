package io.gitlab.jfronny.cavillum.database;

import io.gitlab.jfronny.cavillum.Cfg;
import io.gitlab.jfronny.cavillum.database.migration.Migration;
import io.gitlab.jfronny.cavillum.database.migration.Migration1;
import io.gitlab.jfronny.cavillum.database.migration.Migration2;
import io.gitlab.jfronny.cavillum.database.migration.Migration3;
import io.gitlab.jfronny.cavillum.model.Post;
import io.gitlab.jfronny.cavillum.util.PostMap;
import org.jooq.CloseableDSLContext;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;

import java.io.Closeable;
import java.util.List;
import java.util.stream.Collectors;

import static io.gitlab.jfronny.cavillum.database.KnownFields.*;
import static io.gitlab.jfronny.cavillum.database.TypeMappings.recordToPost;

public class DBConnection implements Closeable {
    private final CloseableDSLContext context;
    private final List<Migration> migrations = List.of(new Migration1(), new Migration2(), new Migration3());

    public long insert(Post post) {
        return context.insertInto(cv_posts)
                .set(attachment_type, post.attachmentType().toString())
                .set(comment, post.comment())
                .set(filename, post.filename())
                .set(post_time, post.postTime())
                .set(subject, post.subject())
                .set(username, post.username())
                .returningResult(id).fetchOne().get(id);
    }

    public Post get(long id) {
        return recordToPost(context.select().from(cv_posts).where(KnownFields.id.eq(id)).fetchOne());
    }

    public PostMap getPage(long page, long pageSize) {
        return context.select().from(cv_posts)
                .orderBy(post_time.desc())
                .limit(pageSize) // Slower offset seek since ID order can no longer be guaranteed
                .offset(page * pageSize)
                .fetchStream()
                .collect(Collectors.toMap(record -> record.get(id), TypeMappings::recordToPost, (x, y) -> y, PostMap::new));
    }

    public int getEntryCount() {
        return context.fetchCount(cv_posts);
    }

    public DBConnection() {
        this.context = DSL.using(Cfg.dbUrl, Cfg.dbUser, Cfg.dbPass);
    }

    public void performMigrations() {
        context.createTableIfNotExists(cv_meta).column(schema_version).execute();
        final int version;
        if (context.fetchCount(cv_meta) == 0) {
            // Not yet initialized
            version = 0;
            context.insertInto(cv_meta, schema_version).values(0).execute();
        }
        else {
            // Fetch schema version from DB
            version = context.select(schema_version).from(cv_meta).fetch().get(0).value1();
        }
        // Perform migrations
        for (Migration migration : migrations) {
            if (version <= migration.getVersion()) {
                context.transaction(cf -> {
                    DSLContext dsl = cf.dsl();
                    migration.perform(dsl);
                    dsl.update(cv_meta).set(schema_version, migration.getVersion()).execute();
                });
            }
        }
    }

    @Override
    public void close() {
        context.close();
    }
}
