package io.gitlab.jfronny.cavillum.formatter.ir;

import java.util.List;

public record SpoilerNode(List<Node> children) implements Node {
    @Override
    public void appendTo(StringBuilder sb) {
        sb.append("<span class=\"spoiler\">");
        for (Node child : children) {
            child.appendTo(sb);
        }
        sb.append("</span>");
    }

    @Override
    public void decompile(StringBuilder sb) {
        sb.append("[spoiler]");
        for (Node child : children) {
            child.decompile(sb);
        }
        sb.append("[/spoiler]");
    }
}
