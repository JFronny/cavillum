package io.gitlab.jfronny.cavillum.formatter.ir;

import java.util.List;

public record ItalicNode(List<Node> children) implements Node {
    @Override
    public void appendTo(StringBuilder sb) {
        sb.append("<i>");
        for (Node child : children) {
            child.appendTo(sb);
        }
        sb.append("</i>");
    }

    @Override
    public void decompile(StringBuilder sb) {
        sb.append("**");
        for (Node child : children) {
            child.decompile(sb);
        }
        sb.append("**");
    }
}
