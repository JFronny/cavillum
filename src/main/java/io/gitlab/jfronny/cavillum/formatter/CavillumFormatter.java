package io.gitlab.jfronny.cavillum.formatter;

import io.gitlab.jfronny.cavillum.formatter.ir.Node;
import io.gitlab.jfronny.cavillum.formatter.ir.StringNode;

import java.util.ArrayList;
import java.util.List;

public class CavillumFormatter {
    public static List<Node> parse(String source) {
        char[] src = source.toCharArray();
        String currentBuf = "";
        List<Node> result = new ArrayList<>();
        for (int i = 0; i < src.length; i++) {
            boolean identified = false;
            for (Token type : Token.values()) {
                if (peek(src, i, type.beginCode)) {
                    int innerStart = i + type.beginCode.length();
                    int innerEnd = source.indexOf(type.endCode, innerStart);
                    if (innerEnd == -1) innerEnd = source.length();
                    else {
                        // Workaround to prevent an italic tag from ending a bold tag
                        if (type == Token.Bold) {
                            while (peek(src, innerEnd, "**")) {
                                innerEnd = source.indexOf(type.endCode, innerEnd + 2);
                                if (innerEnd == -1) {
                                    innerEnd = source.length();
                                    break;
                                }
                            }
                        }
                    }
                    if (!currentBuf.isEmpty()) {
                        result.add(new StringNode(currentBuf));
                        currentBuf = "";
                    }
                    result.add(type.buildNode(source.substring(innerStart, innerEnd)));
                    i = innerEnd + type.endCode.length() - 1;
                    identified = true;
                    break;
                }
            }
            if (!identified) currentBuf += src[i];
        }
        if (!currentBuf.isEmpty()) {
            result.add(new StringNode(currentBuf));
        }
        return result;
    }

    public static String decompile(List<Node> nodes) {
        StringBuilder sb = new StringBuilder();
        for (Node node : nodes) {
            node.decompile(sb);
        }
        String res = sb.toString();
        while (res.startsWith("\n"))
            res = res.substring(1);
        while (res.endsWith("\n"))
            res = res.substring(0, res.length() - 1);
        return res;
    }

    public static String build(String source) {
        return build(parse(source));
    }

    public static String build(List<Node> nodes) {
        StringBuilder sb = new StringBuilder();
        for (Node node : nodes) {
            node.appendTo(sb);
        }
        String res = sb.toString();
        while (res.startsWith("<br>"))
            res = res.substring(4);
        while (res.endsWith("<br>"))
            res = res.substring(0, res.length() - 4);
        res = res.replace("<br>", "<br>\n");
        return res;
    }

    private static boolean peek(char[] src, int i, String expected) {
        if (i + expected.length() >= src.length) return false;
        char[] exp = expected.toCharArray();
        for (int j = 0; j < exp.length; j++) {
            if (src[i + j] != exp[j])
                return false;
        }
        return true;
    }
}
