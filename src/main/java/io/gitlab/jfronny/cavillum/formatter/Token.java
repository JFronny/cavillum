package io.gitlab.jfronny.cavillum.formatter;

import io.gitlab.jfronny.cavillum.formatter.ir.*;

import java.util.function.Function;

public enum Token {
    Greentext("> ", "\n", s -> new GreentextNode(CavillumFormatter.parse(s))),
    Glow("(((", ")))", s -> new GlowNode(CavillumFormatter.parse(s))),
    Spoiler("[spoiler]", "[/spoiler]", s -> new SpoilerNode(CavillumFormatter.parse(s))),
    Italic("**", "**", s -> new ItalicNode(CavillumFormatter.parse(s))),
    Bold("*", "*", s -> new BoldNode(CavillumFormatter.parse(s))),
    Strikethrough("~~", "~~", s -> new StrikethroughNode(CavillumFormatter.parse(s))),
    Hyperlink("[link]", "[/link]", HyperlinkNode::new);

    public final String beginCode;
    public final String endCode;
    private final Function<String, Node> nodeBuilder;

    Token(String beginCode, String endCode, Function<String, Node> nodeBuilder) {
        this.beginCode = beginCode;
        this.endCode = endCode;
        this.nodeBuilder = nodeBuilder;
    }

    public Node buildNode(String source) {
        return nodeBuilder.apply(source);
    }
}
