package io.gitlab.jfronny.cavillum.formatter.ir;

public interface Node {
    void appendTo(StringBuilder sb);
    void decompile(StringBuilder sb);
}
