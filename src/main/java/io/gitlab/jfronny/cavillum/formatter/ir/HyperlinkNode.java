package io.gitlab.jfronny.cavillum.formatter.ir;

import manifold.rt.api.util.ManEscapeUtil;

public record HyperlinkNode(String link) implements Node {
    @Override
    public void appendTo(StringBuilder sb) {
        sb.append("<a href=\"")
                .append(getEscapedLink())
                .append("\">")
                .append(ManEscapeUtil.escapeForHTML(link))
                .append("</a>");
    }

    @Override
    public void decompile(StringBuilder sb) {
        sb.append("[link]").append(link).append("[/link]");
    }

    private String getEscapedLink() {
        return link
                .replace("'", "&apos;")
                .replace("\"", "&quot;")
                .replace("<", "&lt;")
                .replace(">", "&gt;")
                .replace("&", "&amp;")
                .replace(" ", "%20");
    }
}
