package io.gitlab.jfronny.cavillum.formatter.ir;

import java.util.List;

public record StrikethroughNode(List<Node> children) implements Node {
    @Override
    public void appendTo(StringBuilder sb) {
        sb.append("<del>");
        for (Node child : children) {
            child.appendTo(sb);
        }
        sb.append("</del>");
    }

    @Override
    public void decompile(StringBuilder sb) {
        sb.append("~~");
        for (Node child : children) {
            child.decompile(sb);
        }
        sb.append("~~");
    }
}
