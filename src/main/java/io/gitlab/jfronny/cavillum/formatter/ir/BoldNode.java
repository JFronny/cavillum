package io.gitlab.jfronny.cavillum.formatter.ir;

import java.util.List;

public record BoldNode(List<Node> children) implements Node {
    @Override
    public void appendTo(StringBuilder sb) {
        sb.append("<b>");
        for (Node child : children) {
            child.appendTo(sb);
        }
        sb.append("</b>");
    }

    @Override
    public void decompile(StringBuilder sb) {
        sb.append("*");
        for (Node child : children) {
            child.decompile(sb);
        }
        sb.append("*");
    }
}
