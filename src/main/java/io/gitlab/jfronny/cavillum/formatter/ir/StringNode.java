package io.gitlab.jfronny.cavillum.formatter.ir;

import manifold.rt.api.util.ManEscapeUtil;

public record StringNode(String content) implements Node {
    @Override
    public void appendTo(StringBuilder sb) {
        sb.append(ManEscapeUtil.escapeForHTML(content));
    }

    @Override
    public void decompile(StringBuilder sb) {
        sb.append(content);
    }
}
