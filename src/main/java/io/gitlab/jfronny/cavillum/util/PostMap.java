package io.gitlab.jfronny.cavillum.util;

import io.gitlab.jfronny.cavillum.model.Post;

import java.util.LinkedHashMap;

// This class exists because Manifold Templates appear to break with generic params using more than one type (IE Map<Integer, Post>)
public class PostMap extends LinkedHashMap<Long, Post> {
}
