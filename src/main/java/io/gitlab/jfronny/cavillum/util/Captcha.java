package io.gitlab.jfronny.cavillum.util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Random;

public class Captcha {
    private static final Random RNG = new Random();
    private static final char[] TEXT_POSSIBLE = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
    public static String generateText() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < RNG.nextInt(4, 7); i++) sb.append(TEXT_POSSIBLE[RNG.nextInt(TEXT_POSSIBLE.length)]);
        return sb.toString();
    }

    public static byte[] generateImage(String text) throws IOException {
        byte[] bytes = text.getBytes();
        int[] hShifts = new int[bytes.length];
        int w = 20 + text.length() * 20, h = 40;
        for (int i = 0; i < hShifts.length; i++) {
            w += hShifts[i] = RNG.nextInt(-2, 3);
        }

        BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = image.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g.setFont(new Font("Serif", Font.PLAIN, 26));

        // Background
        g.setColor(randomColor(false));
        g.fillRect(0, 0, w, h);

        // Background noise
        for (int i = 0; i < 10; i++) {
            g.setColor(randomColor(false));
            randomOval(g, w, h);
        }

        // Text
        int charLeft = 10;
        for (int i = 0; i < bytes.length; i++) {
            g.setColor(randomColor(true));
            charLeft += hShifts[i];
            g.drawString(new String(new byte[]{bytes[i]}), charLeft, RNG.nextInt(20, 40));
            charLeft += 20;
        }

        // Obstructions
        for (int i = 0; i < 6; i++) {
            g.setColor(randomColor(true));
            randomOval(g, w, h);
        }

        g.dispose();
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream()) {
            ImageIO.write(image, "png", bout);
            return bout.toByteArray();
        }
    }

    private static void randomOval(Graphics2D g, int w, int h) {
        g.drawOval(RNG.nextInt(w - 20) - 20, RNG.nextInt(h) - 10, RNG.nextInt(30, 60), RNG.nextInt(30, 60));
    }

    private static Color randomColor(boolean foreground) {
        float hue = RNG.nextFloat();
        float saturation = RNG.nextFloat() * 0.4f;
        if (foreground) saturation += 0.6f;
        float brightness = foreground ? 1f : (RNG.nextFloat() * 0.4f + 0.2f);
        return new Color(Color.HSBtoRGB(hue, saturation, brightness));
    }
}
