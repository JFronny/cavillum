package io.gitlab.jfronny.cavillum.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FormattingUtil {
    private static final double l1024 = Math.log(1024);
    private static final String[] sizeSuffixes = new String[] {"B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB"};
    private static final SimpleDateFormat rfc822Format = new SimpleDateFormat("EEE, d MMM y HH:mm:ss Z", Locale.ENGLISH);
    public static String formatFileSize(long bytesNo) {
        if (bytesNo < 0) return "-" + formatFileSize(-bytesNo);
        int exp = (int) Math.floor(Math.log(bytesNo) / l1024 + 0.1);
        return formatDecimalRounded(bytesNo / Math.pow(1024, exp)) + " " + sizeSuffixes[exp];
    }

    public static String formatDecimalRounded(double d) {
        return BigDecimal.valueOf(d).setScale(2, RoundingMode.HALF_UP).toPlainString();
    }

    public static String formatTimestampRFC822(Timestamp timestamp) {
        return rfc822Format.format(new Date(timestamp.getTime()));
    }
}
