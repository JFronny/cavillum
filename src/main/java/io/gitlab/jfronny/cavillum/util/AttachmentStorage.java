package io.gitlab.jfronny.cavillum.util;

import io.gitlab.jfronny.cavillum.Cfg;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class AttachmentStorage {
    public static boolean contains(long id) {
        return Files.isRegularFile(toPath(id));
    }

    public static void put(long id, InputStream in) throws IOException {
        try (OutputStream os = Files.newOutputStream(toPath(id))) {
            in.transferTo(os);
        }
    }

    public static InputStream get(long id) throws IOException {
        return Files.newInputStream(toPath(id));
    }

    public static byte[] getBytes(long id) throws IOException {
        return Files.readAllBytes(toPath(id));
    }

    public static long getSize(long id) throws IOException {
        return Files.size(toPath(id));
    }

    public static String getString(long id) throws IOException {
        return Files.readString(toPath(id));
    }

    public static void remove(long id) throws IOException {
        Files.delete(toPath(id));
    }

    private static Path toPath(long id) {
        return Cfg.attachmentsDirectory.resolve(Long.toString(id));
    }
}
