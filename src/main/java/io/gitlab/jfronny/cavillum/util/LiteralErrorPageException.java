package io.gitlab.jfronny.cavillum.util;

import io.javalin.http.Context;
import io.gitlab.jfronny.cavillum.webapp.ErrorPage;

public class LiteralErrorPageException extends RuntimeException {
    private final int code;
    private final String message;
    private final String details;

    public LiteralErrorPageException(int code, String message, String details) {
        super(message);
        this.code = code;
        this.message = message;
        this.details = details;
    }

    public void applyTo(Context ctx) {
        ctx.status(code);
        ctx.html(ErrorPage.render(message, details));
    }
}
