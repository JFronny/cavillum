package io.gitlab.jfronny.cavillum;

import io.gitlab.jfronny.commons.serialize.gson.api.GsonHolder;
import io.gitlab.jfronny.gson.stream.JsonReader;
import io.gitlab.jfronny.gson.stream.JsonToken;
import io.gitlab.jfronny.gson.stream.JsonWriter;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Cfg {
    private static final Path CONFIG_PATH;

    static {
        String c = System.getProperty("cavillum.config");
        if (c == null) {
            Path runDir = Path.of(".").resolve("run").toAbsolutePath();
            if (Files.exists(runDir)) CONFIG_PATH = runDir.resolve("cavillum.json5");
            else {
                throw new RuntimeException("Please set the cavillum.config property to your config directory!");
            }
        } else {
            CONFIG_PATH = Paths.get(c).toAbsolutePath();
        }
    }

    public static int port = 4567;
    public static String subpath = "";
    public static String title = "Cavillum";
    public static int pageSize = 10;
    public static long attachmentSize = 8192;
    public static Path attachmentsDirectory;
    public static String dbUrl = "jdbc:mariadb://localhost:3306/cavillum";
    public static String dbUser = "cavillum";
    public static String dbPass = "some_pass";

    public static void load() throws IOException {
        if (!Files.exists(CONFIG_PATH.getParent()))
            Files.createDirectories(CONFIG_PATH.getParent());
        if (!Files.exists(CONFIG_PATH)) write();
        try (Reader reader = Files.newBufferedReader(CONFIG_PATH);
             JsonReader jr = GsonHolder.getGson().newJsonReader(reader)) {
            jr.beginObject();
            while (jr.peek() != JsonToken.END_OBJECT) {
                String name = null;
                try {
                    name = jr.nextName();
                    switch (name) {
                        case "port" -> port = jr.nextInt();
                        case "subpath" -> subpath = jr.nextString();
                        case "title" -> title = jr.nextString();
                        case "pageSize" -> pageSize = jr.nextInt();
                        case "attachmentSize" -> attachmentSize = jr.nextLong();
                        case "attachmentsDirectory" -> {
                            if (jr.peek() == JsonToken.NULL) {
                                attachmentsDirectory = null;
                                jr.nextNull();
                            } else attachmentsDirectory = Paths.get(jr.nextString()).toAbsolutePath();
                        }
                        case "dbUrl" -> dbUrl = jr.nextString();
                        case "dbUser" -> dbUser = jr.nextString();
                        case "dbPass" -> dbPass = jr.nextString();
                        default -> {
                            Cavillum.LOGGER.error("Unexpected entry name: " + name);
                            jr.skipValue();
                        }
                    }
                } catch (Throwable t) {
                    if (name == null) Cavillum.LOGGER.error("Could not read config entry", t);
                    else Cavillum.LOGGER.error("Could not read config entry: " + name, t);
                    System.exit(0);
                }
            }
            jr.endObject();
        }
        boolean changed = false;
        if (!subpath.isBlank()) {
            if (!subpath.startsWith("/")) {
                changed = true;
                subpath = "/" + subpath;
            }
            if (!subpath.endsWith("/")) {
                changed = true;
                subpath += "/";
            }
        }
        if (changed) write();
        if (attachmentsDirectory == null) {
            throw new RuntimeException("Please configure your instance first");
        }
    }

    public static void write() {
        try (Writer writer = Files.newBufferedWriter(CONFIG_PATH);
             JsonWriter jw = GsonHolder.getGson().newJsonWriter(writer)) {
            jw.beginObject()
                    .comment("---------")
                    .comment("Webserver")
                    .comment("---------")
                    .comment("The port to host this instance under")
                    .name("port").value(port)
                    .comment("The path to host this instances under")
                    .name("subpath").value(subpath)

                    .comment("---------")
                    .comment("Frontend")
                    .comment("---------")
                    .comment("The title of this instance")
                    .name("title").value(title)
                    .comment("The amount of entries to show per page")
                    .name("pageSize").value(pageSize)

                    .comment("---------")
                    .comment("Backend")
                    .comment("---------")
                    .comment("The maximum size of attachments in KiB")
                    .name("attachmentSize").value(attachmentSize)
                    .comment("The directory to write attachments to")
                    .name("attachmentsDirectory").value(attachmentsDirectory == null ? null : attachmentsDirectory.toString())
                    .comment("The JDBC URL of your database (only the mariadb provider is present by default)")
                    .name("dbUrl").value(dbUrl)
                    .comment("The username for your database connection")
                    .name("dbUser").value(dbUser)
                    .comment("The password for your database connection")
                    .name("dbPass").value(dbPass)
                    .endObject();
        } catch (IOException e) {
            Cavillum.LOGGER.error("Could not save config", e);
        }
    }
}
