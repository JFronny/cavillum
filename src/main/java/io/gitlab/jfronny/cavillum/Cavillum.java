package io.gitlab.jfronny.cavillum;

import io.gitlab.jfronny.cavillum.database.DBConnection;
import io.gitlab.jfronny.cavillum.model.AttachmentType;
import io.gitlab.jfronny.cavillum.model.Post;
import io.gitlab.jfronny.cavillum.rss.PostsRss;
import io.gitlab.jfronny.cavillum.util.AttachmentStorage;
import io.gitlab.jfronny.cavillum.util.Captcha;
import io.gitlab.jfronny.cavillum.util.LiteralErrorPageException;
import io.gitlab.jfronny.cavillum.webapp.ErrorPage;
import io.gitlab.jfronny.cavillum.webapp.MainPage;
import io.gitlab.jfronny.cavillum.webapp.PageLayout;
import io.gitlab.jfronny.cavillum.webapp.components.BasicPost;
import io.gitlab.jfronny.commons.log.Level;
import io.gitlab.jfronny.commons.log.Logger;
import io.gitlab.jfronny.commons.log.StdoutLogger;
import io.javalin.Javalin;
import io.javalin.http.ContentType;
import io.javalin.http.Context;
import io.javalin.http.UploadedFile;
import manifold.templates.rt.ManifoldTemplates;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.regex.Pattern;

import static io.javalin.apibuilder.ApiBuilder.*;

//TODO format lists like in markdown
//TODO moderation tools
//TODO fix single post page
//TODO /catalog to view all images
//TODO json api
public class Cavillum {
    public static final Logger LOGGER = Logger.forName("cavillum");
    public static final String CAPTCHA_ATT = "captcha";
    public static final DBConnection POSTS;

    static {
        try {
            Logger.registerFactory(name -> new StdoutLogger(name, true, true, true));
            Cfg.load();
            POSTS = new DBConnection();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) throws IOException {
        // Hides the annoying jooq prompts
        System.setProperty("org.jooq.no-logo", "true");
        System.setProperty("org.jooq.no-tips", "true");

        POSTS.performMigrations();

        Javalin app = Javalin.create(cfg -> {
            cfg.showJavalinBanner = false;
        }).start(Cfg.port);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                LOGGER.info("Gracefully shutting down");
                POSTS.close();
                app.close();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }));

        // Web service
        app.exception(Exception.class, (e, ctx) -> {
            LOGGER.error("Could not process request: " + ctx.path(), e);
            ctx.status(500);
            ctx.html(postFormat(ErrorPage.render(e.getMessage(), e.toString()), ctx));
        });

        app.exception(LiteralErrorPageException.class, LiteralErrorPageException::applyTo);

        ManifoldTemplates.setDefaultLayout(PageLayout.asLayout());

        byte[] favicon = loadStaticResource("favicon.ico", "favicon");
        byte[] rssIcon = loadStaticResource("rss.svg", "RSS icon");
        byte[] stylesCss = loadStaticResource("styles.css", "stylesheet");

        app.routes(() -> path(Cfg.subpath, () -> {
            get(ctx -> ctx.html(renderPage(0, ctx)));

            get("captcha.png", ctx -> {
                String captcha = Captcha.generateText();
                ctx.contentType(ContentType.IMAGE_PNG);
                ctx.sessionAttribute(CAPTCHA_ATT, captcha);
                ctx.result(Captcha.generateImage(captcha));
            });

            get("favicon.ico", ctx -> ctx.contentType(ContentType.IMAGE_ICO).result(favicon));

            get("styles.css", ctx -> ctx.contentType(ContentType.TEXT_CSS).result(stylesCss));

            get("rss.svg", ctx -> ctx.contentType("image/svg+xml").result(rssIcon));
            get("rss.xml", ctx -> ctx.contentType(ContentType.TEXT_XML).result(postFormat(PostsRss.withoutLayout().render(POSTS.getPage(0, Cfg.pageSize)), ctx)));

            get("{page}", ctx -> ctx.html(renderPage(Integer.parseInt(ctx.pathParam("page")), ctx)));

            post("upload", ctx -> {
                if (!ctx.isMultipartFormData()) {
                    ctx.status(400);
                    ctx.html(postFormat(ErrorPage.render("Invalid upload", "Please use the proper upload control on the website"), ctx));
                    return;
                }
                // Get parameters
                String captcha = ctx.formParam("captcha");
                String username = ctx.formParam("username"); //TODO filter username for fancy stuff (soft hyphen, etc)
                String subject = ctx.formParam("subject");
                String comment = ctx.formParam("comment");
                UploadedFile attachment = ctx.uploadedFile("attachment");
                // Validate parameters
                if (captcha == null || !captcha.equals(ctx.sessionAttribute(CAPTCHA_ATT)))
                    throw new LiteralErrorPageException(401, "Invalid captcha", "Please fill in the captcha correctly");
                if (username == null || username.isBlank())
                    throw new LiteralErrorPageException(400, "Invalid username", "Please specify a username");
                if (subject == null || subject.isBlank())
                    throw new LiteralErrorPageException(400, "Invalid subject", "Please specify a subject");
                if (attachment == null || attachment.getSize() == 0)
                    throw new LiteralErrorPageException(400, "Attachment required", "You must provide an attachment file to upload a post");
                if (attachment.getSize() > Cfg.attachmentSize * 1024)
                    throw new LiteralErrorPageException(400, "Invalid attachment", "Please use attachments smaller than 8MiB");
                AttachmentType type = AttachmentType.getByMime(attachment.getContentType());
                if (type == null)
                    throw new LiteralErrorPageException(400, "Invalid attachment", "Please use attachments in one of the supported formats");
                // Save
                Post post = new Post(username, subject, comment, attachment.getFilename(), Timestamp.from(Instant.now()), type);
                long id = POSTS.insert(post);
                AttachmentStorage.put(id, attachment.getContent());
                ctx.sessionAttribute(CAPTCHA_ATT, null);
                ctx.redirect("post/" + id);
            });

            path("post/{id}", () -> {
                get(ctx -> {
                    long id = Long.parseLong(ctx.pathParam("id"));
                    Post post = POSTS.get(id);
                    if (post == null) notFound();
                    ctx.html(postFormat(BasicPost.render(id, post), ctx));
                });

                get("attachment", ctx -> {
                    long id = Long.parseLong(ctx.pathParam("id"));
                    Post post = POSTS.get(id);
                    if (post == null) notFound();
                    if (post.attachmentType() == AttachmentType.YOUTUBE) {
                        ctx.redirect("https://youtu.be/" + AttachmentStorage.getString(id));
                    }
                    else {
                        ctx.contentType(post.attachmentType().getMimeType());
                        ctx.result(AttachmentStorage.get(id));
                    }
                });
            });
        }));

        //TODO allow importing from tinyib
        LOGGER.info("Cavillum is hosted on http://127.0.0.1:" + app.port() + Cfg.subpath);
    }

    private static byte[] loadStaticResource(String fileName, String resourceName) throws IOException {
        try (InputStream is = Cavillum.class.getClassLoader().getResourceAsStream("io/gitlab/jfronny/cavillum/webapp/static/" + fileName)) {
            if (is == null) throw new FileNotFoundException("Could not load included " + resourceName);
            return is.readAllBytes();
        }
    }

    private static final Pattern PAGE_TITLE_PATTERN = Pattern.compile("@@\\{pageTitle}@@"); // Page title macro, needed for the PageLayout
    private static final Pattern HOST_PATH_PATTERN = Pattern.compile("@@\\{hostPath}@@"); // hostname + SUBPATH for use in RSS
    private static final Pattern NEWLINE_SPACE_PATTERN = Pattern.compile(" *\n *"); // Only RSS/HTML is generated, newlines/too much whitespace are unneeded
    private static String postFormat(String formatted, Context ctx) {
        formatted = PAGE_TITLE_PATTERN.matcher(formatted).replaceAll(Cfg.title);
        formatted = HOST_PATH_PATTERN.matcher(formatted).replaceAll(ctx.scheme() + "://" + ctx.host() + Cfg.subpath);
        formatted = NEWLINE_SPACE_PATTERN.matcher(formatted).replaceAll(" ");
        return formatted;
    }

    private static void notFound() {
        throw new LiteralErrorPageException(404, "Not found", "The requested resource could not be found");
    }

    public static String renderPage(int page, Context ctx) {
        int pageCount = Math.max(1, Math.ceilDiv(POSTS.getEntryCount(), Cfg.pageSize));
        if (page >= pageCount) {
            throw new LiteralErrorPageException(404, "Invalid page", "This page does not exist");
        }
        return postFormat(MainPage.render(POSTS.getPage(page, Cfg.pageSize), page, pageCount), ctx);
    }
}
